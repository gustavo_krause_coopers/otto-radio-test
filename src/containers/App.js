import React, { Component } from 'react';
import './App.css';

import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom'

import Book from './Book';
import Users from './Users';


class App extends Component {
  render() {
    return (
      <Router>
        <div className="app">

          <Route exact path="/" component={Book}/>
          <Route exact path="/users" component={Users}/>

        </div>
      </Router>
    );
  }
}

export default App;
