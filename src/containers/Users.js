import React, { Component } from 'react';
import './Users.css';

import firebase from 'firebase/app';
import { Link } from 'react-router-dom'

import BrandHeader from '../components/BrandHeader';


class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      users: [],
    };
  }

  componentDidMount() {
    var self = this;

    firebase.database().ref('users').once('value', function(snapshot) {
      var exists = false;
      var users = [];
      snapshot.forEach(function(childSnapshot){
        users.push(childSnapshot.val());
      })
      self.setState({
        loading: false,
        users: users
      });
    });
  }

  render() {

    const listItems = this.state.users.map((user, index) =>
      <tr key={index}>
        <td>{user.firstName}</td>
        <td>{user.lastName}</td>
        <td>{user.email}</td>
      </tr>
    );

    return (
      <div className="users">
        <div className="users__content">

          <BrandHeader />

          <div className="users__content__container">

            <Link className="button float-right" to="/">Sign up</Link>

            <h1>Users</h1>

            {this.state.loading && (
              <div>
                loading...
              </div>
            )}

            {!this.state.loading && this.state.users && (
              <table>
                <thead>
                  <tr>
                    <th>first name</th>
                    <th>last name</th>
                    <th>e-mail</th>
                  </tr>
                </thead>
                <tbody>
                  {listItems}
                </tbody>
              </table>
            )}

          </div>
        </div>
      </div>
    );
  }
}

export default Users;
