import React, { Component } from 'react';

class Field extends Component {

  constructor(props) {
    super(props);
    this.state = {
      fieldEmpty: true
    };
    this.handleBlur = this.handleBlur.bind(this);
  }

  handleBlur = (event) => {
    this.setState({
      fieldEmpty: false
    });
  }

  render() {

    return (

      <label className={(this.state.fieldEmpty ? 'empty' : '') + ' ' + (this.props.valid ? '' : 'invalid')}>
        {this.props.label}
        <input onBlur={this.handleBlur} {...this.props} />
      </label>

    );
  }
}

export default Field;
