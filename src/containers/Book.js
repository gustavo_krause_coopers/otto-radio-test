import React, { Component } from 'react';
import './Book.css';

import BrandHeader from '../components/BrandHeader';
import SignUp from '../components/SignUp';

class Book extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: 'City of Thieves',
      image: '/images/book.jpg',
      enable: false
    };
  }

  enableBook = () => {
    this.setState({
      enable: true
    });
  }

  render() {
    return (
      <div className="book">
        <div className="book__content">

          <BrandHeader />

          <div className="book__content__container">

            <SignUp book={this.state} onEnableBook={this.enableBook} />

          </div>
        </div>
        <figure className="book__image">
          <img src={process.env.PUBLIC_URL + this.state.image} alt={this.state.title} />
          {this.state.enable && (
            <button className="book__image__play"><span className="show-for-sr">Play</span></button>
          )}
        </figure>
      </div>
    );
  }
}

export default Book;
