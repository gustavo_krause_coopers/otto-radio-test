import React, { Component } from 'react';
import firebase from 'firebase/app';

import './SignUp.css';

import Field from '../components/Field'

import { isRequired, isEmail } from '../helpers'

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      formInvalid: true,
      formSent: false,
      emailRegistered: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange = () => {
    let firstName = document.getElementById('sign-up_firstName').value;
    let lastName = document.getElementById('sign-up_lastName').value;
    let email = document.getElementById('sign-up_email').value;
    let password = document.getElementById('sign-up_password').value;
    this.setState({
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password,
      formInvalid: true
    });

    if(isRequired(firstName) && isRequired(lastName) && isEmail(email) && isRequired(password)) {
      this.setState({
        formInvalid: false
      });
    }
  }

  handleSubmit = () => {
    var self = this;

    this.setState({
      loading: true
    });

    firebase.database().ref('users').once('value', function(snapshot) {
      var exists = false;
      snapshot.forEach(function(childSnapshot){
        if(self.state.email === childSnapshot.val().email){
          exists = true;
        }
      })
      self.setState({
        loading: false
      });
      if(!exists) {
        self.saveUser();
      } else {
        self.emailRegistered();
      }
    });
  }

  saveUser = () => {
    var newUser = firebase.database().ref('users').push();
    newUser.set({
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
      password: this.state.password
    });
    this.setState({
      formSent: true
    });
    this.props.onEnableBook();
  }

  emailRegistered = () => {
    this.setState({
      emailRegistered: true
    });
  }

  render() {

    let validEmail = isEmail(this.state.email) && !this.state.emailRegistered;

    return (
      <div className="sign-up">

        {this.state.loading && (
          <div>
            loading...
          </div>
        )}

        {!this.state.loading && !this.state.formSent && (
          <form onSubmit={this.handleSubmit} className="sign-up__form">
            <fieldset>
              <header>
                <h1>Listen to {this.props.book.title} free now.</h1>
                <p className="lead">Try Otto Radio Unlimited free for 7 days. <strong>Cancel anytime.</strong></p>
              </header>

              <div className="group">
                <Field label="First name" type="text" id="sign-up_firstName" value={this.state.firstName} valid={(isRequired(this.state.firstName) ? 1 : 0)} onChange={this.handleChange} />
                <Field label="Last name" type="text" id="sign-up_lastName" value={this.state.lastName} valid={(isRequired(this.state.lastName) ? 1 : 0)} onChange={this.handleChange} />
              </div>

              <Field label="E-mail" type="email" id="sign-up_email" value={this.state.email} valid={(validEmail ? 1 : 0)} onChange={this.handleChange} />
              {this.state.emailRegistered && this.state.email && (
                <div className="help text--error">
                  This email is already in use.
                </div>
              )}

              <Field label="Password" type="password" id="sign-up_password" value={this.state.password} valid={(isRequired(this.state.password) ? 1 : 0)} onChange={this.handleChange} />

              <footer>
                <button className="button" type="submit" disabled={this.state.formInvalid}>Sign Up</button>
              </footer>

            </fieldset>
          </form>
        )}

        {!this.state.loading && this.state.formSent && (
          <div className="sign-up__feedback">
            <header>
              <p className="text--sucess">Great, you are all set.</p>
              <h1>{this.state.firstName}, you can start listening to {this.props.book.title} now.</h1>
              <p>You can also get the free app to listen offline.</p>
            </header>
            <aside className="text-center">
              <a className="button--appstore" href="https://itunes.apple.com/us/app/otto-radio-podcasts-and-news/id975745769" target="_blank" rel="noopener noreferrer"><span className="show-for-sr">Available on the AppStore</span></a>
            </aside>
          </div>
        )}

      </div>
    );
  }
}

export default SignUp;
