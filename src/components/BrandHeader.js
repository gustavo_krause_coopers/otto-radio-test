import React from 'react';
import './BrandHeader.css';

const BrandHeader = () => (
  <header className="brand-header">
    <h2 className="brand"><a href="https://www.ottoradio.com/"><span className="show-for-sr">otto radio</span></a></h2>
  </header>
);

export default BrandHeader;
