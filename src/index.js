import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App';
import registerServiceWorker from './registerServiceWorker';

import firebase from 'firebase/app';
// import 'firebase/auth';
import 'firebase/database';

// Initialize Firebase
const config = {
    apiKey: "AIzaSyASXrTzY1A-ItCDrNmBON2Vq7S-tlGGrFo",
    authDomain: "otto-d01aa.firebaseapp.com",
    databaseURL: "https://otto-d01aa.firebaseio.com",
    projectId: "otto-d01aa",
    storageBucket: "otto-d01aa.appspot.com",
    messagingSenderId: "507811352278"
};
firebase.initializeApp(config);

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
